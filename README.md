# IDIoT

Instantly Deploy-able Internet of Things

A DIY (Do It Yourself) platform for the emergence of a trend of people seeking knowledge & self improvement in IoT through small projects as a part of academics, creativity, hobby, experimentation or cost-saving.

IDIoT simplifies SaaS for IoT, with no cloud expertise required.

IDIoT is a centrally managed global solution that makes it easy to connect, monitor (Display Sensor Value and Actuator Status) and control (ON/OFF Toggle) ONE IoT asset per Email ID without the aid of experts and professionals.

IDIoT is open-source and made out of Academic Research to make IoT Cloud Deployment simple to learn and experiment.

http://idiot.epizy.com
